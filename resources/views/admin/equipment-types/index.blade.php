@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [])
            @slot('title') Список типов оборудования @endslot
        @endcomponent

        <a href="{{ route('admin.equipment-type.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
            Добавить тип оборудования</a>

        <table class="table table-striped table-bordered mt-3">
            <thead>
            <tr>
                <th>Название</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($equipmentTypes as $equipmentType)
                <tr>
                    <td><a href="{{ route('admin.equipment-type.show', $equipmentType) }}">{{ $equipmentType->title }}</a></td>
                    <td class="text-right">
                        <form action="{{ route('admin.equipment-type.destroy', $equipmentType) }}"
                              onsubmit="return confirm('Удалить?');" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a href="{{ route('admin.equipment-type.edit', $equipmentType) }}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn btn-link"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="2" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3">
                    <ul class="pagination pull-right">
                        {{ $equipmentTypes->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
