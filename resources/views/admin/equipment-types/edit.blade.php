@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.equipment-type.index' => 'Список типов оборудования'
            ])
            @slot('title') Редактирование типа оборудования @endslot
        @endcomponent

        <form action="{{ route('admin.equipment-type.update', $equipmentType) }}" method="post" class="form-horizontal">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.equipment-types.partials.form')

        </form>

    </div>

@endsection
