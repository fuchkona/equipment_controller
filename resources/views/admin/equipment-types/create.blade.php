@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.equipment-type.index' => 'Список типов оборудования'
            ])
            @slot('title') Создание типа оборудования @endslot
        @endcomponent

        <form action="{{ route('admin.equipment-type.store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}

            @include('admin.equipment-types.partials.form')

        </form>

    </div>

@endsection
