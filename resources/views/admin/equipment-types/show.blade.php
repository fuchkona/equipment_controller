@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.equipment-type.index' => 'Список типов оборудования'
            ])
            @slot('title') Тип оборудования {{ $equipmentType->title }} @endslot
        @endcomponent

        <p>Заголовок: {{ $equipmentType->title }}</p>

    </div>

@endsection
