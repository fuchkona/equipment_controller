@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [])
            @slot('title') Список типов задач @endslot
        @endcomponent

        <a href="{{ route('admin.task-type.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
            Добавить тип задачи</a>

        <table class="table table-striped table-bordered mt-3">
            <thead>
            <tr>
                <th>Название</th>
                <th>Описание</th>
                <th>Начальный статус</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($taskTypes as $taskType)
                <tr>
                    <td><a href="{{ route('admin.task-type.show', $taskType) }}">{{ $taskType->title }}</a></td>
                    <td>{{ $taskType->description }}</td>
                    <td>{{ $taskType->startStatus->title }} - <small>{{ $taskType->startStatus->description }}</small></td>
                    <td class="text-right">
                        <form action="{{ route('admin.task-type.destroy', $taskType) }}"
                              onsubmit="return confirm('Удалить?');" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a href="{{ route('admin.task-type.edit', $taskType) }}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn btn-link"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4">
                    <ul class="pagination pull-right">
                        {{ $taskTypes->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
