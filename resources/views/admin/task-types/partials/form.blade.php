<div class="form-group">
    <label>Название</label>
    <input class="form-control" type="text" name="title" required="required" value="{{ $taskType->title }}">

    <label>Описание</label>
    <textarea class="form-control" type="text" name="description">{{ $taskType->description }}
    </textarea>

    <label>Начальный статус</label>
    <select class="form-control" name="start_status_id" required="required">
        @foreach($statuses as $status)
            <option value="{{ $status->id }}"
                    @if($status->id == $taskType->start_status_id) selected="selected" @endif>{{ $status->title }}
                - {{ $status->description }}</option>
        @endforeach
    </select>
</div>
<input type="submit" class="btn btn-primary" value="Сохранить">
