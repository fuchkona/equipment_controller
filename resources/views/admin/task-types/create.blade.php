@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.task-type.index' => 'Список типов задач'
            ])
            @slot('title') Создание типа задачи @endslot
        @endcomponent

        <form action="{{ route('admin.task-type.store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}

            @include('admin.task-types.partials.form')

        </form>

    </div>

@endsection
