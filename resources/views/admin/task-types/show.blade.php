@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.task-type.index' => 'Список типов задач'
            ])
            @slot('title') Тип задачи {{ $taskType->title }} @endslot
        @endcomponent

        <p>Заголовок: {{ $taskType->title }}</p>
        <p>Описание: {{ $taskType->description }}</p>
        <p>Начальный статус: {{ $taskType->startStatus->title }} - <small>{{ $taskType->startStatus->description }}</small></p>

    </div>

@endsection
