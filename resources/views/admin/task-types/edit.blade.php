@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.task-type.index' => 'Список типов задач'
            ])
            @slot('title') Редактирование типа задачи @endslot
        @endcomponent

        <form action="{{ route('admin.task-type.update', $taskType) }}" method="post" class="form-horizontal">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.task-types.partials.form')

        </form>

    </div>

@endsection
