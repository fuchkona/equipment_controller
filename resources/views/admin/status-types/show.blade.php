@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.status-type.index' => 'Список типов статусов'
            ])
            @slot('title') Тип статуса {{ $statusType->title }} @endslot
        @endcomponent

        <p>Заголовок: {{ $statusType->title }}</p>
        <p>Следующий: {{ $statusType->nextStatus->title }} - <small>{{ $statusType->nextStatus->description }}</small></p>
        <p>Автоматическое переключение: @if($statusType->autoNext) Да @else Нет @endif</p>
        <p>Требуемые поля: {{ $statusType->requiredString() }}</p>
        <p>Отмена: {{ $statusType->cancelStatus->title }} - <small>{{ $statusType->cancelStatus->description }}</small></p>
        <p>Вызов функции при смене статуса: {{ $statusType->callback }}</p>

    </div>

@endsection
