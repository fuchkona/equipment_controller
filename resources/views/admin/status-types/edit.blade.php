@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.status-type.index' => 'Список типов стстусов'
            ])
            @slot('title') Редактирование типа стстусов @endslot
        @endcomponent

        <form action="{{ route('admin.status-type.update', $statusType) }}" method="post" class="form-horizontal">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.status-types.partials.form')

        </form>

    </div>

@endsection
