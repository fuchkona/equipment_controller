@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [])
            @slot('title') Список типов статусов @endslot
        @endcomponent

        <a href="{{ route('admin.status-type.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
            Добавить тип статуса</a>

        <table class="table table-striped table-bordered mt-3">
            <thead>
            <tr>
                <th>Название</th>
                <th>Описание</th>
                <th>Следующий</th>
                <th>Автоматическое переключение</th>
                <th>Требуемые поля</th>
                <th>Отмена</th>
                <th>Вызов функции при смене статуса</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($statusTypes as $statusType)
                <tr>
                    <td><a href="{{ route('admin.status-type.show', $statusType) }}">{{ $statusType->title }}</a></td>
                    <td>{{ $statusType->description }}</td>
                    <td>{{ $statusType->nextStatus->title }} - <small>{{ $statusType->nextStatus->description }}</small></td>
                    <td>@if($statusType->autoNext) Да @else Нет @endif</td>
                    <td>{{ $statusType->requiredString() }}</td>
                    <td>{{ $statusType->cancelStatus->title }} - <small>{{ $statusType->cancelStatus->description }}</small></td>
                    <td>{{ $statusType->callback }}</td>
                    <td class="text-right">
                        <form action="{{ route('admin.status-type.destroy', $statusType) }}"
                              onsubmit="return confirm('Удалить?');" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a href="{{ route('admin.status-type.edit', $statusType) }}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn btn-link"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="8" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="8">
                    <ul class="pagination pull-right">
                        {{ $statusTypes->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
