@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.status-type.index' => 'Список типов стстусов'
            ])
            @slot('title') Создание типа статуса @endslot
        @endcomponent

        <form action="{{ route('admin.status-type.store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}

            @include('admin.status-types.partials.form')

        </form>

    </div>

@endsection
