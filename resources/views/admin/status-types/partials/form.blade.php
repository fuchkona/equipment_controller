<div class="form-group">
    <label>Название</label>
    <input class="form-control" type="text" name="title" required="required" value="{{ $statusType->title }}">

    <label>Описание</label>
    <input class="form-control" type="text" name="description" value="{{ $statusType->description }}">

    <label>Следующий</label>
    <select class="form-control" name="next">
        <option value="">Нет</option>
        @foreach($statuses as $status)
            <option value="{{ $status->id }}"
                    @if($status->id == $statusType->next) selected="selected" @endif>{{ $status->title }} - {{ $status->description }}</option>
        @endforeach
    </select>

    <div class="form-group mt-3">
        <div class="form-check">
            <label class="form-check-label">
                <input type="hidden" name="auto_next" value="0">
                <input class="form-check-input" type="checkbox" name="auto_next" value="1"
                       @if($statusType->autoNext) checked="checked" @endif>
                Автоматическое переключение
            </label>
        </div>
    </div>

    <label>Требуемые поля</label>
    <div class="form-group">
        @foreach(\App\StatusType::$requiredFields as $requiredFieldKey => $requiredField)
            <div class="form-check">
                <label class="form-check-label">
                    <input name="required[]" class="form-check-input" type="checkbox" value="{{ $requiredFieldKey }}" @if($statusType->isRequired($requiredFieldKey)) checked="checked" @endif>
                    {{ $requiredField }}
                </label>
            </div>
        @endforeach
    </div>

    <label>Отмена</label>
    <select class="form-control" name="cancel">
        <option value="">Нет</option>
        @foreach($statuses as $status)
            <option value="{{ $status->id }}"
                    @if($status->id == $statusType->cancel) selected="selected" @endif>{{ $status->title }} - {{ $status->description }}</option>
        @endforeach
    </select>

    <label>Вызов функции при смене статуса</label>
    <input class="form-control" type="text" name="callback" value="{{ $statusType->callback }}">
</div>
<input type="submit" class="btn btn-primary" value="Сохранить">
