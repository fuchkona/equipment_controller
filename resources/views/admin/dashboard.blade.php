@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="jumbotron justify-content-center">
                    <h5>Задач - <span class="badge badge-success">{{ $tasksCount }}</span></h5>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <h5>Торговых точек - <span class="badge badge-success">{{ $storesCount }}</span></h5>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <h5>Агенств - <span class="badge badge-success">{{ $agenciesCount }}</span></h5>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <h5>Видов товаров - <span class="badge badge-success">{{ $equipmentTypesCount }}</span><h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 mb-5">
                <a href="{{ route('admin.task.create') }}" class="btn btn-block btn-success mb-2"><i class="fa fa-plus"></i> Добавить задачу</a>
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">Последние задачи</h4>
                    <hr>
                    @forelse ($lastTasks as $task)
                        <p class="list-group-item-text"><a href="{{ route('admin.task.show', $task) }}">{{ $task->title }}</a></p>
                    @empty
                        <p class="list-group-item-text">Данные отсутствуют</p>
                    @endforelse
                </div>
            </div>
            <div class="col-sm-6 mb-5">
                <a href="{{ route('admin.store.create') }}" class="btn btn-block btn-success mb-2"><i class="fa fa-plus"></i> Добавить торговую точку</a>
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">Последние торговые точки</h4>
                    <hr>
                    @forelse ($lastStores as $store)
                        <p class="list-group-item-text"><a href="{{ route('admin.store.show', $store) }}">{{ $store->title }}</a></p>
                    @empty
                        <p class="list-group-item-text">Данные отсутствуют</p>
                    @endforelse
                </div>
            </div>
            <div class="col-sm-6 mb-5">
                <a href="{{ route('admin.agency.create') }}" class="btn btn-block btn-success mb-2"><i class="fa fa-plus"></i> Добавить агентство</a>
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">Последние агентства</h4>
                    <hr>
                    @forelse ($lastAgencies as $agency)
                        <p class="list-group-item-text"><a href="{{ route('admin.agency.show', $agency) }}">{{ $agency->title }}</a></p>
                    @empty
                        <p class="list-group-item-text">Данные отсутствуют</p>
                    @endforelse
                </div>
            </div>
            <div class="col-sm-6 mb-5">
                <a href="{{ route('admin.equipment-type.create') }}" class="btn btn-block btn-success mb-2"><i class="fa fa-plus"></i> Добавить вид товара</a>
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">Последние виды товаров</h4>
                    <hr>
                    @forelse ($lastEquipmentTypes as $equipmentType)
                        <p class="list-group-item-text"><a href="{{ route('admin.equipment-type.show', $equipmentType) }}">{{ $equipmentType->title }}</a></p>
                    @empty
                        <p class="list-group-item-text">Данные отсутствуют</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection
