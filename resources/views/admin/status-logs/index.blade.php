@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [])
            @slot('title') История статусов @endslot
        @endcomponent

        <table class="table table-striped table-bordered mt-3">
            <thead>
            <tr>
                <th>Задача</th>
                <th>Статус</th>
                <th>Создан</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($statusLogs as $statusLog)
                <tr>
                    <td>{{ $statusLog->task->title }}</td>
                    <td>{{ $statusLog->statusType->title }}</td>
                    <td>{{ $statusLog->created_at->format('d.m.Y H:i:s') }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3">
                    <ul class="pagination pull-right">
                        {{ $statusLogs->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
