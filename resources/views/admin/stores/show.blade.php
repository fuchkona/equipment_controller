@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.store.index' => 'Список торговых точек'
            ])
            @slot('title') Торговая точка {{ $store->title }} @endslot
        @endcomponent

        <p>Заголовок: {{ $store->title }}</p>

    </div>

@endsection
