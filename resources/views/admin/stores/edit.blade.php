@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.store.index' => 'Список торговых точек'
            ])
            @slot('title') Редактирование торговой точки @endslot
        @endcomponent

        <form action="{{ route('admin.store.update', $store) }}" method="post" class="form-horizontal">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.stores.partials.form')

        </form>

    </div>

@endsection
