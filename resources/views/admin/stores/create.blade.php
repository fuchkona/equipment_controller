@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.store.index' => 'Список торговых точек'
            ])
            @slot('title') Создание торговой точки @endslot
        @endcomponent

        <form action="{{ route('admin.store.store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}

            @include('admin.stores.partials.form')

        </form>

    </div>

@endsection
