@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.agency.index' => 'Список агентств'
            ])
            @slot('title') Редактирование агентства @endslot
        @endcomponent

        <form action="{{ route('admin.agency.update', $agency) }}" method="post" class="form-horizontal">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.agencies.partials.form')

        </form>

    </div>

@endsection
