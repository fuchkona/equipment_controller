@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.agency.index' => 'Список агентств'
            ])
            @slot('title') Агентство {{ $agency->title }} @endslot
        @endcomponent

        <p>Заголовок: {{ $agency->title }}</p>

    </div>

@endsection
