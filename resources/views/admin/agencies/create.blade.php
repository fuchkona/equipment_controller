@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.agency.index' => 'Список агентств'
            ])
            @slot('title') Создание агентства @endslot
        @endcomponent

        <form action="{{ route('admin.agency.store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}

            @include('admin.agencies.partials.form')

        </form>

    </div>

@endsection
