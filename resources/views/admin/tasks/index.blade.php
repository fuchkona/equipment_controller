@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [])
            @slot('title') Список Задач @endslot
        @endcomponent
        <div class="row">
            <div class="col">
                <a href="{{ route('admin.task.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
                    Добавить задачу</a>
            </div>
            <div class="col">
                <form action="{{ route('admin.task.index') }}" method="GET">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Поиск"
                               value="{{ $search  }}">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-striped table-bordered mt-3">
            <thead>
            <tr>
                <th>Название</th>
                <th>Тип</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($tasks as $task)
                <tr>
                    <td><a href="{{ route('admin.task.show', $task) }}">{{ $task->title }}</a></td>
                    <td>{{ $task->type->title }}</td>
                    <td class="text-right">
                        <form action="{{ route('admin.task.destroy', $task) }}" onsubmit="return confirm('Удалить?');"
                              method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a href="{{ route('admin.task.edit', $task) }}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn btn-link"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3">
                    <ul class="pagination pull-right">
                        {{ $tasks->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
