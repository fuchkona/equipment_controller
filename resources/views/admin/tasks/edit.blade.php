@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.task.index' => 'Список задач'
            ])
            @slot('title') Редактирование задачи @endslot
        @endcomponent

        <form action="{{ route('admin.task.update', $task) }}" method="post" class="form-horizontal">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.tasks.partials.form')

        </form>

    </div>

@endsection
