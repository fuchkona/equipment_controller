@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.task.index' => 'Список задач'
            ])
            @slot('title') Создание задачи @endslot
        @endcomponent

        <form action="{{ route('admin.task.store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}

            @include('admin.tasks.partials.form')

        </form>

    </div>

@endsection
