@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('pages', [
                'admin.task.index' => 'Список задач'
            ])
            @slot('title') Задача {{ $task->title }} @endslot
        @endcomponent

        <div class="form-inline">
            <a class="btn btn-primary mr-2" href="{{ route('admin.task.edit', $task) }}"><i class="fa fa-edit"></i>
                Редактировать</a>
            @if($task->canGoNext())
                <form action="{{ route('admin.task.go-next', $task) }}" class="mr-2"
                      onsubmit="return confirm('Перевести на следующий этап?');" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success"><i class="fa fa-arrow-right"></i> Перевести на
                        следующий этап
                    </button>
                </form>
            @endif
            @if($task->canCancel())
                <form action="{{ route('admin.task.cancel', $task) }}" class="mr-2"
                      onsubmit="return confirm('Отменить статус?');" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-warning"><i class="fa fa-times"></i> Отменить статус
                    </button>
                </form>
            @endif
        </div>

        <hr>

        <div class="row">
            <div class="col">
                <p><strong>Заголовок:</strong> {{ $task->title }}</p>
                <p><strong>Тип задачи:</strong> {{ $task->type->title }}</p>
                <p><strong>Торговая точка:</strong> {{ $task->store->title }}</p>
                <p><strong>Торговая точка назначения:</strong> {{ $task->destinationStore->title }}</p>
                <p><strong>Дата события:</strong> {{ $task->date }}</p>
                <p><strong>Логистический код:</strong> {{ $task->logisticCode }}</p>
                <p><strong>Тип оборудования:</strong> {{ $task->equipmentType->title }}</p>
                <p><strong>Агентство:</strong> {{ $task->agency->title }}</p>
                <p><strong>Код оплаты заказа:</strong> {{ $task->orderCode }}</p>
                <p><strong>Оценка:</strong> {{ $task->mark }}</p>
                <p><strong>Комментарий оценки:</strong></p>
                <p>{{ $task->comment }}</p>
            </div>
            <div class="col">
                <p><strong>Текущий статус:</strong> {{ $task->lastStatus->title }}</p>
                <p><strong>Следующий статус:</strong> {{ $task->lastStatus->nextStatus->title }}</p>
                <p><strong>Статус отмены:</strong> {{ $task->lastStatus->cancelStatus->title }}</p>
                <p><strong>Требуемые поля для перехода на следующий статус:</strong></p>
                <p>{{ $task->lastStatus->requiredString() }}</p>
                <p><strong>Переход на следующий:</strong> @if($task->canGoNext()) Возможен @else Невозможен @endif</p>
            </div>
        </div>
    </div>

@endsection
