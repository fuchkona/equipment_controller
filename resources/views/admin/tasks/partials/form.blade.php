<div class="form-group">
    <div class="form-group">
        <label>Название</label>
        <input class="form-control" type="text" name="title" required="required" value="{{ $task->title }}">
    </div>

    <div class="form-group">
        <label>Тип задачи</label>
        <select class="form-control" name="type_id" required="required">
            @foreach($taskTypes as $taskType)
                <option value="{{ $taskType->id }}"
                        @if($taskType->id == $task->typeId) selected="selected" @endif>{{ $taskType->title }}
                    - {{ $taskType->description }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Торговая точка</label>
        <select class="form-control" name="store_id">
            <option value="">Нет</option>
            @foreach($stores as $store)
                <option value="{{ $store->id }}"
                        @if($store->id == $task->storeId) selected="selected" @endif>{{ $store->title }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Торговая точка назначения</label>
        <select class="form-control" name="destination_store_id">
            <option value="">Нет</option>
            @foreach($stores as $store)
                <option value="{{ $store->id }}"
                        @if($store->id == $task->destinationStoreId) selected="selected" @endif>{{ $store->title }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Дата события</label>
        <input class="form-control" type="date" name="date" value="{{ $task->date }}">
    </div>

    <div class="form-group">
        <label>Логистический код</label>
        <input class="form-control" type="number" name="logistic_code" value="{{ $task->logisticCode }}">
    </div>

    <div class="form-group">
        <label>Тип оборудования</label>
        <select class="form-control" name="equipment_type_id">
            <option value="">Нет</option>
            @foreach($equipmentTypes as $equipmentType)
                <option value="{{ $equipmentType->id }}"
                        @if($equipmentType->id == $task->equipmentTypeId) selected="selected" @endif>{{ $equipmentType->title }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Агентство</label>
        <select class="form-control" name="agency_id">
            <option value="">Нет</option>
            @foreach($agencies as $agency)
                <option value="{{ $agency->id }}"
                        @if($agency->id == $task->agencyId) selected="selected" @endif>{{ $agency->title }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Код оплаты заказа</label>
        <input class="form-control" type="text" name="order_code" value="{{ $task->orderCode }}">
    </div>

    <div class="form-group">
        <label>Оценка</label>
        <input min="0" max="5" class="form-control" type="number" name="mark" value="{{ $task->mark }}" id="mark">
    </div>

    <div class="form-group">
        <label>Комментарий оценки</label>
        <input class="form-control" type="text" name="comment" value="{{ $task->comment }}" id="comment">
    </div>
</div>
<input type="submit" class="btn btn-primary" value="Сохранить">

