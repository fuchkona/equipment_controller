<h2>{{ $title }}</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Главная</a></li>
    @foreach($pages as $pageUrl => $pageTitle)
        <li class="breadcrumb-item"><a href="{{ route($pageUrl) }}">{{ $pageTitle }}</a></li>
    @endforeach
    <li class="breadcrumb-item active">{{ $title }}</li>
</ol>
<hr>
