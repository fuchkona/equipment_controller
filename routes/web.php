<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::resource('/store', 'StoreController', ['as' => 'admin']);
    Route::resource('/agency', 'AgencyController', ['as' => 'admin']);
    Route::resource('/equipment-type', 'EquipmentTypeController', ['as' => 'admin']);
    Route::resource('/task', 'TaskController', ['as' => 'admin']);
    Route::post('/task/go-next/{task}', 'TaskController@goNext')->name('admin.task.go-next');
    Route::post('/task/cancel/{task}', 'TaskController@cancel')->name('admin.task.cancel');
    Route::resource('/task-type', 'TaskTypeController', ['as' => 'admin']);
    Route::resource('/status-type', 'StatusTypeController', ['as' => 'admin']);
    Route::resource('/status-log', 'StatusLogController', ['as' => 'admin']);
});

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
