<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('type_id');
            $table->integer('store_id')->nullable();
            $table->integer('destination_store_id')->nullable();
            $table->date('date')->nullable();
            $table->integer('logistic_code')->nullable();
            $table->integer('equipment_type_id')->nullable();
            $table->integer('agency_id')->nullable();
            $table->string('order_code')->nullable();
            $table->tinyInteger('mark')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
