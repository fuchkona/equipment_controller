<?php

namespace App\Listeners;

use App\Events\onUpdateTask;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateTask
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onUpdateTask  $event
     * @return void
     */
    public function handle(onUpdateTask $event)
    {
        $task = $event->task;

        if ($task->lastStatus->auto_next && $task->canGoNext()) {
            $task->goNext();
        }
    }
}
