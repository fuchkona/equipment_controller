<?php

namespace App\Listeners;

use App\Events\onAddTask;
use App\StatusLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddTask
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onAddTask  $event
     * @return void
     */
    public function handle(onAddTask $event)
    {
        $slog = new StatusLog();
        $slog->task_id = $event->task->id;
        $slog->status_type_id = $event->task->type->start_status_id;
        $slog->save();
    }
}
