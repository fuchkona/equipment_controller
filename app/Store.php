<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Store
 *
 * @mixin \Eloquent
 */
class Store extends BaseModel
{

    protected $fillable = ['title'];

}
