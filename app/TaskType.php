<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskType extends BaseModel
{
    protected $fillable = ['title', 'description', 'start_status_id'];

    public function startStatus() {
        return $this->hasOne(StatusType::class,'id', 'start_status_id')->withDefault();
    }
}
