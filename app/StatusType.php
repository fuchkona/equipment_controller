<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class StatusType extends BaseModel
{
    protected $fillable = ['title', 'description', 'auto_next', 'required', 'next', 'cancel', 'callback'];
    public static $requiredFields = [
        'store_id' => 'Торговая точка',
        'destination_store_id' => 'Торговая точка назначения',
        'agency_id' => 'Агентство',
        'date' => 'Дата события',
        'logistic_code' => 'Логистический код',
        'equipment_type_id' => 'Тип оборудования',
        'order_code' => 'Оплата счета',
        'mark' => 'Оценка',
        'comment' => 'Комментарий оценки',
    ];

    public function nextStatus() {
        return $this->hasOne(self::class, 'id', 'next')->withDefault(['title' => 'Нет']);
    }

    public function cancelStatus() {
        return $this->hasOne(self::class, 'id', 'cancel')->withDefault(['title' => 'Нет']);
    }

    /**
     * @param $key
     * @return bool is field $key is required
     */
    public function isRequired($key) {
        return in_array($key, $this->requiredArray());
    }

    public function requiredArray() {
        return explode(',', $this->required);
    }

    public function requiredString() {
        $result = [];
        foreach ($this->requiredArray() as $item) {
            $result[] = array_get(self::$requiredFields, $item);
        }
        return join(', ', $result);
    }

}
