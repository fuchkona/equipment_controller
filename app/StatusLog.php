<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusLog extends BaseModel
{
    protected $fillable = ['task_id', 'status_type_id'];

    public function task() {
        return $this->hasOne(Task::class, 'id', 'task_id')->withDefault();
    }

    public function statusType() {
        return $this->hasOne(StatusType::class, 'id', 'status_type_id')->withDefault();
    }
}
