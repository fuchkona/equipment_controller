<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Agency
 *
 * @mixin \Eloquent
 */
class Agency extends BaseModel
{
    protected $fillable = ['title'];
}
