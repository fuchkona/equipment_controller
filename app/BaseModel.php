<?php
/**
 * Created by PhpStorm.
 * User: fuchkona
 * Date: 17.10.2018
 * Time: 10:52
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function getAttribute($key)
    {

        if (array_key_exists($key, $this->relations) || method_exists($this, $key)) {
            return parent::getAttribute($key);
        } else {
            return parent::getAttribute(snake_case($key));
        }
    }

    public function setAttribute($key, $value)
    {
        return parent::setAttribute(snake_case($key), $value);
    }
}