<?php

namespace App;

use App\Events\onAddTask;
use App\Events\onUpdateTask;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package App
 *
 * @property TaskType $type
 * @property StatusLog $lastStatusLog
 * @property StatusType $lastStatus
 */
class Task extends BaseModel
{
    protected $fillable = [
        'title',
        'type_id',
        'store_id',
        'destination_store_id',
        'date',
        'logistic_code',
        'equipment_type_id',
        'agency_id',
        'order_code',
        'mark',
        'comment',
    ];

    protected $dispatchesEvents = [
        'created' => onAddTask::class,
        'updated' => onUpdateTask::class,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->hasOne(TaskType::class, 'id', 'type_id')->withDefault();
    }

    public function store()
    {
        return $this->hasOne(Store::class, 'id', 'store_id')->withDefault();
    }

    public function destinationStore()
    {
        return $this->hasOne(Store::class, 'id', 'destination_store_id')->withDefault();
    }

    public function equipmentType()
    {
        return $this->hasOne(EquipmentType::class, 'id', 'equipment_type_id')->withDefault();
    }

    public function agency()
    {
        return $this->hasOne(Agency::class, 'id', 'agency_id')->withDefault();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastStatusLog()
    {
        return $this->hasOne(StatusLog::class, 'task_id', 'id')->latest()->withDefault();
    }

    public function lastStatus()
    {
        return $this->lastStatusLog->hasOne(StatusType::class, 'id', 'status_type_id')->withDefault();
    }

    public function canCancel()
    {
        $status = $this->lastStatus;
        if (empty($status->cancel)) {
            return false;
        }
        return true;
    }

    public function cancel()
    {
        if ($this->canCancel()) {
            $slog = new StatusLog();
            $slog->task_id = $this->id;
            $slog->status_type_id = $this->lastStatus->cancel;
            $slog->save();
        }
    }

    public function canGoNext()
    {
        $status = $this->lastStatus;

        if (empty($status->next)) {
            return false;
        }

        if (empty($status->required)) {
            return true;
        }

        foreach ($status->requiredArray() as $required) {
            if (empty($this->$required)) {
                return false;
            }
        }

        return true;
    }

    public function goNext()
    {
        if ($this->canGoNext()) {
            $slog = new StatusLog();
            $slog->task_id = $this->id;
            $slog->status_type_id = $this->lastStatus->next;
            $slog->save();
        }
    }

}
