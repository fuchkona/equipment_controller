<?php

namespace App\Http\Controllers\Admin;

use App\Agency;
use App\EquipmentType;
use App\Store;
use App\Task;
use App\TaskType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request|null $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = Task::query();
        $search = Input::get('search');

        if ($search) {
            $task->where('title', 'like',"%$search%");
        }

        return view('admin.tasks.index', [
            'tasks' => $task->paginate(10),
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tasks.create', [
            'task' => new Task(),
            'taskTypes' => TaskType::get(),
            'stores' => Store::get(),
            'equipmentTypes' => EquipmentType::get(),
            'agencies' => Agency::get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Task::create($request->all());

        return redirect()->route('admin.task.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return view('admin.tasks.show', [
            'task' => $task,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('admin.tasks.edit', [
            'task' => $task,
            'taskTypes' => TaskType::get(),
            'stores' => Store::get(),
            'equipmentTypes' => EquipmentType::get(),
            'agencies' => Agency::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $mark = $request->get('mark');
        $comment = $request->get('comment');
        if ($mark == 5 && empty($comment)) {
            $comment = '-';
        }

        $task->update(array_merge($request->except('comment'), ['comment' => $comment]));

        return redirect()->route('admin.task.show', $task);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();

        return redirect()->route('admin.task.index');
    }

    public function goNext(Task $task)
    {
        if ($task->canGoNext()) {
            $task->goNext();
        }

        return redirect()->route('admin.task.show', $task);
    }

    public function cancel(Task $task)
    {
        if ($task->canCancel()) {
            $task->cancel();
        }

        return redirect()->route('admin.task.show', $task);
    }

}
