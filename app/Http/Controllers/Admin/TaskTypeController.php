<?php

namespace App\Http\Controllers\Admin;

use App\StatusType;
use App\TaskType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.task-types.index',[
           'taskTypes' => TaskType::paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.task-types.create', [
            'taskType' => new TaskType(),
            'statuses' => StatusType::get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        TaskType::create($request->all());

        return redirect()->route('admin.task-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function show(TaskType $taskType)
    {
        return view('admin.task-types.show',[
            'taskType' => $taskType,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskType $taskType)
    {
        return view('admin.task-types.edit', [
            'taskType' => $taskType,
            'statuses' => StatusType::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskType $taskType)
    {
        $taskType->update($request->all());

        return redirect()->route('admin.task-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskType $taskType)
    {
        $taskType->delete();

        return redirect()->route('admin.task-type.index');
    }
}
