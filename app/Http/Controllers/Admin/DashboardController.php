<?php

namespace App\Http\Controllers\Admin;

use App\EquipmentType;
use App\Store;
use App\Agency;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    // dashboard
    public function dashboard() {
        return view('admin.dashboard',[
            'lastStores' => Store::orderBy('id', 'desc')->take(5)->get(),
            'lastAgencies' => Agency::orderBy('id', 'desc')->take(5)->get(),
            'lastEquipmentTypes' => EquipmentType::orderBy('id', 'desc')->take(5)->get(),
            'lastTasks' => Task::orderBy('id', 'desc')->take(5)->get(),
            'storesCount' => Store::count(),
            'agenciesCount' => Agency::count(),
            'equipmentTypesCount' => EquipmentType::count(),
            'tasksCount' => Task::count(),
        ]);
    }
}
