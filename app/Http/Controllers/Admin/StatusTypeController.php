<?php

namespace App\Http\Controllers\Admin;

use App\StatusType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.status-types.index', [
            'statusTypes' => StatusType::orderBy('id', 'desc')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.status-types.create', [
            'statusType' => new StatusType(),
            'statuses' => StatusType::get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        StatusType::create(array_merge($request->except(['required']), ['required' => join(',', $request->get('required',[]))]));

        return redirect()->route('admin.status-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatusType $statusType
     * @return \Illuminate\Http\Response
     */
    public function show(StatusType $statusType)
    {
        return view('admin.status-types.show', [
            'statusType' => $statusType,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatusType $statusType
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusType $statusType)
    {
        return view('admin.status-types.edit', [
            'statusType' => $statusType,
            'statuses' => StatusType::where('id', '!=', $statusType->id)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\StatusType $statusType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusType $statusType)
    {

        $statusType->update(array_merge($request->except(['required']), ['required' => join(',', $request->get('required',[]))]));

        return redirect()->route('admin.status-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatusType $statusType
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusType $statusType)
    {
        $statusType->delete();

        return redirect()->route('admin.status-type.index');
    }
}
